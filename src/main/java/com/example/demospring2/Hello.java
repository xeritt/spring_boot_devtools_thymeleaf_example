package com.example.demospring2;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Hello {
    class Person{
        public String name;
        public int age;

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public Person get(@PathVariable String id) {
        return new Person(id, 12);
    }
}