package com.example.demospring2;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
public class Greeting {
    //@Value("${welcome.message:test}")
    private String message = "Hello World";

    @RequestMapping(value = "/greeting")
    public String index(Map<String, Object> model) {
        model.put("message1", this.message);
        model.put("message2", "It work!");
        return "greeting";
    }
}
